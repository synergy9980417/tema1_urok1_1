//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.
//        System.out.printf("Hello and welcome!");
//
//        for (int i = 1; i <= 5; i++) {
//            //TIP Press <shortcut actionId="Debug"/> to start debugging your code. We have set one <icon src="AllIcons.Debugger.Db_set_breakpoint"/> breakpoint
//            // for you, but you can always add more by pressing <shortcut actionId="ToggleLineBreakpoint"/>.
//            System.out.println("i = " + i);
//        }
        System.out.println("__________________________________________________________");
        System.out.println("задание от Сенергия 1_1");
//        4. Вывести значение 2 + 2 * 2
        System.out.println(2+3*2);
//        5. Вывести значение (2+2)*2
        System.out.println(2+2*2);
//        6. Вывести значение деления 100 на пи
        System.out.println(100/Math.PI);
//        7. Вывести
//        значение 12345 в третьей степени
        System.out.println(Math.pow(12345, 3));
//        8. Вывести квадратный корень от двух в 10 степени
        System.out.println(Math.sqrt(Math.pow(2,10)));
//        9. Вывести корень из двух, возведенный в 10 степень
        System.out.println(Math.pow(Math.sqrt(2),10));
//        10. Что будет, если в джаве поделить на ноль? Проверить
//        System.out.println(1/0);
//        11. Попробуйте сложить две строки в Java. Выведите результат.
//        1
        System.out.println("a"+"b");
//        2. Попробуйте вычесть, разделить две строки.
//        System.out.println("ab"-"c");
//        System.out.println("ab"/"c");
//        13. Попробуйте сложить строку с числом пи. Что получилось?
        System.out.println("PI="+Math.PI);
//        14. Полезное упражнение: напишите программу, которая считает корень
//        линейного уравнения ax+b=0
        System.out.println(Math.PI);
//        15. Полезное упражнение: напишите программу, которая счит
//        ает корни
//        уравнения (ax+b)*(cx+d)=0
        int a,x,b,c,d;
        a=-1;b=0;c=1;d=1;
//        Раскройте скобки:
//        (ax+b)*(cx+d) = acx^2 + (ad+bc)x + bd
//        Приравняйте полученное произведение к нулю:
//        acx^2 + (ad+bc)x + bd = 0
//        Это квадратное уравнение вида ax^2 + bx + c = 0, где
//        a = ac
//        b = ad + bc
//        c = bd
//        Найдите дискриминант D = b^2 - 4ac
        double D=Math.pow(b,2)-4*a*c;
        System.out.println("D = "+D);
//        Если D > 0, уравнение имеет два различных корня:
//        x1 = (-b + sqrt(D))/(2a)
//        x2 = (-b - sqrt(D))/(2a)
//        Если D = 0, уравнение имеет один корень:
//        x = -b/(2a)
//        Если D < 0, уравнение не имеет вещественных корней.
        double x1,x2;
        if ( D > 0 )
        {
            x1 = (-b + Math.sqrt(D))/(2*a);
            x2 = (-b - Math.sqrt(D))/(2*a);
            System.out.println("x1 = " + x1);
            System.out.println("x2 = " + x2);
        }
        else if ( D < 0 )
        System.out.println("уравнение не имеет вещественных корней");
        else
        {
            double x3;
            x3 = -b/(2*a);
            System.out.println("x3="+x3);
        }

//        16. Полезное упражнение: напишите программу, которая считает дискриминант
//        квадратного уравнения

//        уже написано в предыдущем задании

    }
}